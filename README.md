# Ansible role - Deploy CLI
This role provides installation of Cryton CLI using Docker or pip.  
First, it checks if the appropriate user is created, installs Docker or pip and then installs the app.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```
- Ansible variables (do not disable `gather_facts` directive)

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

It is recommended to update the `cryton_cli_environment` variable.

For example:
```yaml
      cryton_cli_environment:
        MY_VARIABLE: my_value
```

| variable                        | description                                                                                                                                                       |
|---------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cryton_cli_git_repository       | Origin repository.                                                                                                                                                |
| cryton_cli_repository_branch    | Which version to use.                                                                                                                                             |
| cryton_cli_version              | Which version to use.                                                                                                                                             |
| run_as_user                     | Which user to use/create.                                                                                                                                         |
| cryton_cli_app_directory        | App directory path.                                                                                                                                               |
| cryton_cli_executable_directory | Path to the directory which contains the executable.                                                                                                              |
| cryton_cli_installation         | Type of the installation. Possible options are `pip` and `docker`.                                                                                                |
| cryton_cli_environment          | Dictionary with the settings. More information can be found [here](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/components/cli/#settings). |

## Examples

### Usage
**Pip installation:**
```yaml
  roles:
    - role: deploy-cli
      become: yes

```

**Docker installation:**
```yaml
  roles:
    - role: deploy-cli
      become: yes
      run_as_user: root
      cryton_cli_installation: docker

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: deploy-cli
  src: https://gitlab.ics.muni.cz/cryton/ansible/deploy-cli.git
  version: "master"
  scm: git
```
